## v1.5.0
* Anpassung an Foundry v11
* letzte gefundene Fehler korrigiert
* 2 neue Szenen mit der reparierten Festung Altarein (ab Kapitel 4). ACHTUNG: in Kapitel 5 und 6 sind die Szenen auch enthalten und werden daher die beiden Szenen aus dem vorherigen Kapitel importierten überschreiben!
