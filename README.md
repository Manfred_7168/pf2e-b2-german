# pf2e-b2-german

Öffentliches Projekt des MHB2 German von Ulisses Spiele

## Fehler melden

Falls sie Fehler im Foundry-Modul Monsterhandbuch 2 feststellen, melden sie diese bitte in diesem Projekt unter dem Punkt Issues.
Geben sie dazu bitte folgende Informationen an:

- Names des Monsters
- Name der Fähigkeit oder des Gegenstands
- Was ist falsch?
- Wie müsste es richtig sein/was erwarten sie?

